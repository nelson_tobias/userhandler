const axios = require('axios')
var logger = require('../../es/logger')

const getWebSites = async (req, res) => {
  logger.info('Requisitando usuarios ')
  const clientlist = await axios.get('https://jsonplaceholder.typicode.com/users')

  logger.info('Recuperando nome de todos websites (getWebSites)')
  const webSites = clientlist.data.map(function (client) {
    return client.website
  })

  return res.send(
    webSites
  )
}

module.exports = {
  getWebSites
}
