const axios = require('axios')
var logger = require('../../es/logger')

const getInfo = async (req, res) => {
  logger.info('Requisitando usuarios ')
  const clientlist = await axios.get('https://jsonplaceholder.typicode.com/users')

  logger.info('Iniciando processamento das informacoes (getInfo)')
  const infoClients = clientlist.data.map(function (client) {
    return [client.name, client.email, client.company]
  })

  return res.send(
    infoClients.sort()
  )
}

module.exports = {
  getInfo
}
