const express = require('express')
const bodyParser = require('body-parser')
const app = express()
var logger = require('./es/logger')

const { handlerRoutes } = require('./handler/routes')

app.use(bodyParser.json())

app.use('/', handlerRoutes)

app.listen(8080, function () {
  logger.info('Api up on port 8080')
})

module.exports = {
  app
}
