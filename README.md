# userhandler
API backend tratamento de uma lista de usuários

# executando
## com docker-compose
> docker-compose build  
> docker-compose up

## local
> npm install   
> node app.js

# running tests
> npm test

# Rotas:

Requisito 1
GET:
http://localhost:8080/v1/websites

Requisito 2
GET:
http://localhost:8080/v1/info

Requisito 3
GET:
http://localhost:8080/v1/suite




