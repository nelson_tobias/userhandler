const axios = require('axios')
var logger = require('../../es/logger')

const getSuiteText = async (req, res) => {
  logger.info('Requisitando usuarios ')
  const clientlist = await axios.get('https://jsonplaceholder.typicode.com/users')

  logger.info('Procurando nomes de quartos com suite (getSuiteText)')
  const adressWithSuite = clientlist.data.filter(function (client) {
    return client.address.suite.toLowerCase().includes('suite')
  })

  return res.send(
    adressWithSuite
  )
}

module.exports = {
  getSuiteText
}
